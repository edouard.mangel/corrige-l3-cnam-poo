﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Games;

public class MorpionGames
{
    private GameTicTacToe game { get; set; }
    private readonly Player p1;
    private readonly Player p2;
    public MorpionGames()
    {
        game = new GameTicTacToe();
        p1 = new Player("Player 1", 'X');
        p2 = new Player("Player 2", 'O');
    }

    private void StartGame()
    {
        game.AddPlayer(p1);
        game.AddPlayer(p2);
        game.Start();
    }

    [Fact]
    public void GameCanOnlyStartWith2Players()
    {
        game.AddPlayer(p1);
        Assert.Throws<Exception>(() => game.Start());
    }

    [Fact]
    public void FirstPlayerShouldBeTheOneToStart()
    {
        StartGame();
        Assert.Equal(p1, game.CurrentPlayer);
    }

    [Fact]
    public void AfterPlayingCurrentPlayerShouldChange()
    {
        StartGame();

        game.PlaceToken(new Position(2, 2));
        Assert.Equal(p2, game.CurrentPlayer);
        game.PlaceToken(new Position(1, 1));
        Assert.Equal(p1, game.CurrentPlayer);
    }

    [Fact]
    public void CanOnlyPlayInEmptySquare()
    {
        StartGame();

        game.PlaceToken(new Position(2, 2));

        Assert.Throws<NonEmptyCellException>(() => game.PlaceToken(new Position(2, 2)));
    }

    [Fact]
    public void PlayerWinsIfHeOwns3squaresOnFirstLine()
    {
        StartGame();

        game.PlaceToken(new Position(1, 1));
        game.PlaceToken(new Position(2, 1));
        game.PlaceToken(new Position(1, 2));
        game.PlaceToken(new Position(2, 2));
        game.PlaceToken(new Position(1, 3));
        Assert.False(game.IsPlaying);
        Assert.Equal(game.Winner, p1);
    }

    [Fact]
    public void PlayerWinsIfHeOwns3squaresOnLastLine()
    {
        StartGame();

        game.PlaceToken(new Position(3, 1));
        game.PlaceToken(new Position(2, 1));
        game.PlaceToken(new Position(3, 2));
        game.PlaceToken(new Position(2, 2));
        game.PlaceToken(new Position(3, 3));
        Assert.False(game.IsPlaying);
        Assert.Equal(game.Winner, p1);
    }

    [Fact]
    public void PlayerWinsIfHeOwns3squaresOnAColumn()
    {
        StartGame();

        game.PlaceToken(new Position(1, 1));
        game.PlaceToken(new Position(1, 2));
        game.PlaceToken(new Position(2, 1));
        game.PlaceToken(new Position(2, 2));
        game.PlaceToken(new Position(3, 1));
        Assert.False(game.IsPlaying);
        Assert.Equal(game.Winner, p1);
    }

    [Fact]
    public void GameOverWithDrasIfGridIsFull()
    {
        StartGame();
        game.PlaceToken(new Position(1, 1));
        game.PlaceToken(new Position(2, 1));
        game.PlaceToken(new Position(3, 1));
        game.PlaceToken(new Position(2, 2));
        game.PlaceToken(new Position(2, 3));
        game.PlaceToken(new Position(1, 3));
        game.PlaceToken(new Position(3, 3));
        game.PlaceToken(new Position(3, 2));
        game.PlaceToken(new Position(1, 2));
        Assert.False(game.IsPlaying);
        Assert.Null(game.Winner);
    }
    [Fact]
    public void GameEndsIfPlayerOwnsRightDiagonal()
    {
        StartGame();
        game.PlaceToken(new Position(1, 1));
        game.PlaceToken(new Position(2, 1));
        game.PlaceToken(new Position(2, 2));
        game.PlaceToken(new Position(2, 3));
        game.PlaceToken(new Position(3, 3));
        Assert.False(game.IsPlaying);
        Assert.Equal(p1, game.Winner);
    }
    [Fact]
    public void GameEndsIfPlayerOwnsLeftDiagonal()
    {
        StartGame();
        game.PlaceToken(new Position(3, 1));
        game.PlaceToken(new Position(2, 1));
        game.PlaceToken(new Position(2, 2));
        game.PlaceToken(new Position(2, 3));
        game.PlaceToken(new Position(1, 3));
        Assert.False(game.IsPlaying);
        Assert.Equal(p1, game.Winner);
    }
}
