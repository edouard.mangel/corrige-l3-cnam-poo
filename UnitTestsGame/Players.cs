﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace UnitTestsGame;

public class Players
{
    private static readonly string player1Name = "Player 1";
    private static readonly string player2Name = "Player 2";

    private static readonly char player1Token = 'X';
    private static readonly char player2Token = 'O';

    [Fact]
    public void ShouldHaveAName()
    {
        Player player1 = new Player(player1Name, 'X');

        Assert.Equal(player1Name, player1.Name);        
    }

    [Fact]
    public void ShouldHaveAToken()
    {
        Player player1 = new Player(player1Name, player1Token);

        Assert.Equal(player1Token, player1.Token);
    }

    [Fact]
    public void ShouldBeAbleToPlayOnEmptySquare()
    {

    }
}
