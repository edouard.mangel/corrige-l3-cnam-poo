﻿using System;
using UnitTestsGame.Builders;
using Xunit;

namespace Games;

public class Connect4Games
{
    private GameConnect4 Game { get; set; }

    private readonly Player p1;
    private readonly Player p2;

    public Connect4Games()
    {
        Game = new GameConnect4();
        p1 = new Player("Player 1", 'X');
        p2 = new Player("Player 2", 'O');
    }

    private void StartGame()
    {
        Game.AddPlayer(p1);
        Game.AddPlayer(p2);
        Game.Start();
    }

    private void StartCustomGame(GameConnect4 game)
    {
        Game = game;
        Game.AddPlayer(p1);
        Game.AddPlayer(p2);
        Game.Start();
    }

    [Fact]
    public void GameCanOnlyStartWith2Players()
    {
        Game.AddPlayer(p1);
        Assert.Throws<Exception>(() => Game.Start());
    }

    [Fact]
    public void FirstPlayerShouldBeTheOneToStart()
    {
        StartGame();
        Assert.Equal(p1, Game.CurrentPlayer);
    }

    [Fact]
    public void GameCantHaveMoreThan2Players()
    {
        Game.AddPlayer(p1);
        Game.AddPlayer(p2);
        Game.AddPlayer(new Player("Player 3", 'Y'));
        Game.Start();
        Assert.Equal(Game.CurrentPlayer, p1);
        Game.PlaceToken(1);
        Assert.Equal(Game.CurrentPlayer, p2);
        Game.PlaceToken(2);
        Assert.Equal(Game.CurrentPlayer, p1);
    }

    [Fact]
    public void DropTokenInColumnPlaysInFirstEmptySquare()
    {
        StartGame();
        Game.PlaceToken(1);
        Assert.Equal(p1.Token, Game.Grid.GetSquare(new Position(1, 1)).Value);
        Game.PlaceToken(1);
        Assert.Equal(p2.Token, Game.Grid.GetSquare(new Position(2, 1)).Value);
        Game.PlaceToken(2);
        Assert.Equal(p1.Token, Game.Grid.GetSquare(new Position(1, 2)).Value);
    }

    [Fact]
    public void CannotPlayInColumnWhenFull()
    {
        StartCustomGame(TestDataConnect4Builder.WithFirstColumnFull());

        Assert.Throws<ColumnFullException>(() => Game.PlaceToken(1));
    }

    [Theory]
    [InlineData("XXX")]
    [InlineData("OXXX")]
    [InlineData("OOXXX")]
    public void PlayerWinsWhenConnects4onColumn(string columnRepresentation)
    {
        StartCustomGame(TestDataConnect4Builder.WithFirstColumn(columnRepresentation));
        Assert.True(Game.IsPlaying);

        Game.PlaceToken(1);
        Assert.False(Game.IsPlaying);
        Assert.Equal(p1, Game.Winner);
    }

    [Theory]
    [MemberData(nameof(GridPuissance4Builder.LineAlmostFull), MemberType = typeof(GridPuissance4Builder))]
    public void PlayerWinsWhenConnects4onLine(GameConnect4 game, int columnToPlay)
    {
        StartCustomGame(game);
        Assert.True(Game.IsPlaying);
        Game.PlaceToken(columnToPlay);
        Assert.False(Game.IsPlaying);
        Assert.Equal(p1, Game.Winner);
    }


    [Theory]
    [MemberData(nameof(GridPuissance4Builder.ColumnAlmostFull), MemberType = typeof(GridPuissance4Builder))]
    public void PlayerWinsWhenConnects4onDiagonal(GameConnect4 game, int columnToPlay)
    {
        StartCustomGame(game);
        Assert.True(Game.IsPlaying);
        Game.PlaceToken(columnToPlay);
        Assert.False(Game.IsPlaying);
        Assert.Equal(p1, Game.Winner);
    }


    [Theory]
    [MemberData(nameof(GridPuissance4Builder.Draw), MemberType = typeof(GridPuissance4Builder))]
    public void Draw(GameConnect4 game, int columnToPlay)
    {
        StartCustomGame(game);
        Assert.True(Game.IsPlaying);
        Game.PlaceToken(columnToPlay);
        Assert.False(Game.IsPlaying);
        Assert.Null(Game.Winner);
    }
}

