﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Games;

namespace UnitTestsGame;

public class GridsConnect4
{
    GridConnect4 grid { get; set; }

    public GridsConnect4()
    {
        grid = new GridConnect4();
    }

    [Fact]
    public void ShouldHaveSizeOf6By7()
    {
        var grid = new GridConnect4();
        
        Assert.Throws<IndexOutOfRangeException>(()=> grid.GetLine(0));
        Assert.Throws<IndexOutOfRangeException>(()=> grid.GetColumn(0));

        Assert.Throws<IndexOutOfRangeException>(()=> grid.GetLine(7));
        Assert.Throws<IndexOutOfRangeException>(()=> grid.GetColumn(8));
    }


    [Fact]
    public void CanRecoverDiagonals()
    {
        var grid = new GridConnect4();
        var diagonals = grid.GetDiagonals(4);
        Assert.True(diagonals.All(d => d.Count >= 4));
        Assert.Equal(12, diagonals.Count);
    }

}
