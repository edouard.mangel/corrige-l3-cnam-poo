﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Games;

namespace UnitTestsGame.Builders;

static class TestDataConnect4Builder
{
    private class TestGamePuissance4 : GameConnect4
    {
        public TestGamePuissance4(GridConnect4 grid)
        {
            Grid = grid;
        }
    }

    private static List<object[]> FromPositions(List<KeyValuePair<Dictionary<int, string>, int>> positionsAndNextMove)
    {
        var data = new List<object[]>();
        foreach (var positions in positionsAndNextMove)
        {
            GridConnect4 grid = GridPuissance4Builder.FromPositions(positions.Key);
            data.Add(new object[] { new TestGamePuissance4(grid), positions.Value });
        }
        return data;
    }
    public static List<object[]> WithFirstLineAlmostFull()
    {
        var positions = GridPuissance4Builder.AlignOnFirstLine();
        return FromPositions(positions);
    }

    public static List<object[]> Draw()
    {
        var positions = GridPuissance4Builder.PrepareDraw();
        return FromPositions(positions);
    }

    public static List<object[]> WithColumnAlmostFull()
    {
        var positions = GridPuissance4Builder.PrepareColumn();
        return FromPositions(positions);
    }

    public static GameConnect4 WithFirstColumnFull()
    {
        var positions = new Dictionary<int, string>();
        positions.Add(1, "XOXOXO");
        return new TestGamePuissance4(GridPuissance4Builder.FromPositions(positions));
    }

    public static GameConnect4 WithFirstColumn(string tokens)
    {
        var positions = new Dictionary<int, string>();
        positions.Add(1, tokens);
        return new TestGamePuissance4(GridPuissance4Builder.FromPositions(positions));
    }
}
