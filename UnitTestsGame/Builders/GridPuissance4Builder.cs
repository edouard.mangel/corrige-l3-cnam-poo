﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Games;

namespace UnitTestsGame.Builders;

public static class GridPuissance4Builder
{
    public static IEnumerable<object[]> LineAlmostFull()
    {
        return TestDataConnect4Builder.WithFirstLineAlmostFull();
    }

    public static IEnumerable<object[]> ColumnAlmostFull()
    {
        return TestDataConnect4Builder.WithColumnAlmostFull();
    }

    public static IEnumerable<object[]> Draw()
    {
        return TestDataConnect4Builder.Draw();
    }

    public static List<KeyValuePair<Dictionary<int, string>, int>> AlignOnFirstLine()
    {
        return new List<KeyValuePair<Dictionary<int, string>, int>>() {
            new KeyValuePair<Dictionary<int, string>, int>(LinePosition1, 1),
            new KeyValuePair<Dictionary<int, string>, int>(LinePosition1, 5),
            new KeyValuePair<Dictionary<int, string>, int>(LinePosition2, 2),
            new KeyValuePair<Dictionary<int, string>, int>(LinePosition2, 6),
            new KeyValuePair<Dictionary<int, string>, int>(LinePosition3, 3),
            new KeyValuePair<Dictionary<int, string>, int>(LinePosition3, 7),
            new KeyValuePair<Dictionary<int, string>, int>(LinePosition4, 4)
        };
    }

    public static List<KeyValuePair<Dictionary<int, string>, int>> PrepareDraw()
    {
        return new List<KeyValuePair<Dictionary<int, string>, int>>() {
            new KeyValuePair<Dictionary<int, string>, int>(DrawPosition, 7),
        };
    }

    public static List<KeyValuePair<Dictionary<int, string>, int>> PrepareColumn()
    {
        return new List<KeyValuePair<Dictionary<int, string>, int>>() {
            new KeyValuePair<Dictionary<int, string>, int>(DiagonalPosition1, 1),
            new KeyValuePair<Dictionary<int, string>, int>(DiagonalPosition1, 5),
            new KeyValuePair<Dictionary<int, string>, int>(DiagonalPosition2, 1)
        };
    }

    public static GridConnect4 FromPositions(Dictionary<int, string> positions)
    {
        var grid = new GridConnect4();
        foreach (var column in positions.Keys)
        {
            string tokens = positions[column];
            foreach (char token in tokens)
            {
                grid.PlayInColumn(column, new Player("", token));
            }
        }
        return grid;
    }

    private static readonly Dictionary<int, string> LinePosition1 = new Dictionary<int, string>(){
        {1, "" },{2, "XO" },{3, "XO" },{4, "XO" },{5, "" },{6, "" },{7, "" }
    };
    private static readonly Dictionary<int, string> LinePosition2 = new Dictionary<int, string>(){
        {1, "" },{2, "" },{3, "XO" },{4, "XO" },{5, "XO" },{6, "" },{7, "" }
    };
    private static readonly Dictionary<int, string> LinePosition3 = new Dictionary<int, string>(){
        {1, "" },{2, "" },{3, "" },{4, "XO" },{5, "XO" },{6, "XO" },{7, "" }
    };
    private static readonly Dictionary<int, string> LinePosition4 = new Dictionary<int, string>(){
        {1, "" },{2, "" },{3, "" },{4, "" },{5, "XO" },{6, "XO" },{7, "XO" },};
    
    private static readonly Dictionary<int, string> DiagonalPosition1 = new Dictionary<int, string>(){
        {1, "" },
        {2, "OXO" },
        {3, "XOX" },
        {4, "XOOX" },
        {5, "OXXO" },{6, "" },{7, "" }
    };

    private static readonly Dictionary<int, string> DiagonalPosition2 = new Dictionary<int, string>(){
        {1, "OXXO" },
        {2, "XOOX" },
        {3, "XOX" },
        {4, "OXO" },
        {5, "" },
        {6, "" },{7, "" }
    };

    private static readonly Dictionary<int, string> DrawPosition = new Dictionary<int, string>(){
        {1, "OOOXXX" },
        {2, "XXXOOO" },
        {3, "OOOXXX" },
        {4, "XXXOOO" },
        {5, "OOOXXX" },
        {6, "XXXOOO" },
        {7, "OOOXX" },
    };

}
