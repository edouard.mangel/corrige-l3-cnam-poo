using System;
using Morpion;
using Xunit;

namespace UnitTestsGame;
public class Morpion
{
    private GridMorpion grid { get; set; }
    public Morpion()
    {
        grid = new GridMorpion();
    }

    [Fact]
    public void ShouldContain3Lines()
    {
        Line firstLine = grid.GetLine(1);
        Line secondLine = grid.GetLine(2);
        Line lastLine = grid.GetLine(3);
        Assert.NotEqual(firstLine, secondLine);
        Assert.NotEqual(lastLine, secondLine);
        Assert.NotEqual(lastLine, firstLine);
    }

    [Fact]
    public void ShouldContain3Columns()
    {
        Column firstColumn = grid.GetColumn(1);
        Column secondColumn = grid.GetColumn(2);
        Column lastColumn = grid.GetColumn(3);
        Assert.NotEqual(firstColumn, secondColumn);
        Assert.NotEqual(lastColumn, secondColumn);
        Assert.NotEqual(lastColumn, firstColumn);
    }
    [Fact]
    public void ShouldContain3ColumnsOnly()
    {
        Assert.Throws<IndexOutOfRangeException>(() => grid.GetColumn(0));
        Assert.Throws<IndexOutOfRangeException>(() => grid.GetColumn(4));
    }

    [Fact]
    public void ShouldContain3LinessOnly()
    {
        Assert.Throws<IndexOutOfRangeException>(() => grid.GetLine(0));
        Assert.Throws<IndexOutOfRangeException>(() => grid.GetLine(4));
    }

    [Fact]
    public void LinesShouldContainIndexes()
    {
        var firstLine = grid.GetLine(1);
        var lastLine = grid.GetLine(grid.NUMBER_OF_LINES);

        Assert.Equal(1, firstLine.Index);
        Assert.Equal(grid.NUMBER_OF_LINES, lastLine.Index);
    }

    [Fact]
    public void ColumnsShouldContainIndexes()
    {
        var firstColumn = grid.GetColumn(1);
        var lastColumn = grid.GetColumn(grid.NUMBER_OF_COLUMNS);

        Assert.Equal(1, firstColumn.Index);
        Assert.Equal(grid.NUMBER_OF_COLUMNS, lastColumn.Index);
    }

    [Fact]
    public void ShouldContainSquares()
    {
        Position p11 = new Position(1, 1);
        Square s1 = grid.GetSquare(p11);

        Assert.Equal(p11, s1.Position);

        Position p33 = new Position(1, 1);
        Square s33 = grid.GetSquare(p33);

        Assert.Equal(p33, s33.Position);
    }

    [Fact]
    public void ShouldContainSameSquaresAsGrid()
    {
        Square s11 = grid.GetSquare(new Position(1, 1));
        Square s11bis = grid.GetLine(1).GetSquare(1);

        Assert.Equal(s11, s11bis);
    }

    [Fact]
    public void EmptySquareShouldContainSpace()
    {
        Position position = new Position(1, 1);
        var s = grid.GetSquare(position);

        Assert.True(s.IsEmpty);
    }

}
