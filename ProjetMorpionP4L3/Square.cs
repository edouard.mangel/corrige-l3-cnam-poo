﻿using System.Collections.Generic;

namespace Morpion;

public class Square
{    
    public Position Position { get; private set; }
    public char Value { get; private set; }
    public bool IsEmpty => Value == ' ';

    public Square(Position position)
    {
        Position = position;
        Value = ' '; 
    }

}