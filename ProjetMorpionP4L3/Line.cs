﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Morpion;

public class Line
{
    public readonly int Index;
    internal List<Square> squares { get; set; } = new List<Square>();

    public Line(int index)
    {
        Index = index;
    }

    public Square GetSquare(int v)
    {
        if (v < 1 || v > squares.Count )
        {
            throw new IndexOutOfRangeException();
        }

        return squares.ElementAt(v-1);
    }
}