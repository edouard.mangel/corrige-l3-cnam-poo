﻿using System.Collections.Generic;

namespace Morpion;

public class Column
{
    public List<Square> squares { get; private set; } = new List<Square>();

    public int Index { get; private set; }

    public Column(int index)
    {
        Index = index;
    }

}