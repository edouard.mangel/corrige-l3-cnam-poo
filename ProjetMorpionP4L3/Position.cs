﻿namespace Morpion;

public struct Position
{
    public readonly int Line;
    public readonly int Column;

    public Position(int v1, int v2)
    {
        Line = v1;
        Column = v2;
    }

    public override string ToString()
    {
        return $"Line : {Line}, Column : {Column}";
    }
}