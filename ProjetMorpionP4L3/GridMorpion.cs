﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Morpion;

public class GridMorpion
{
    public List<Line> _lines { get; private set; } = new List<Line>();
    public List<Column> _columns { get; private set; } = new List<Column>();

    public int NUMBER_OF_LINES => 3;

    public int NUMBER_OF_COLUMNS => 3; 

    public GridMorpion()
    {
        InitLines();
        InitColumns();
    }

    private void InitColumns()
    {
        for (int i = 1; i <= NUMBER_OF_COLUMNS; i++)
        {
            Column column = new Column(i);
            for (int j = 1; j <= NUMBER_OF_LINES; j++)
            {
                Square square = GetLine(j).GetSquare(i);
                column.squares.Add(square);
            }

            _columns.Add(column);
        }
    }

    private void InitLines()
    {
        for (int i = 1; i <= NUMBER_OF_LINES; i++)
        {
            Line currentLine = new Line(i);
            for (int j = 1; j <= NUMBER_OF_COLUMNS; j++)
            {
                Square currentSquare = new Square(new Position(i, j));
                currentLine.squares.Add(currentSquare);
            }
            _lines.Add(currentLine);
        }
    }

    public Line GetLine(int index)
    {
        if (index < 1 || index > NUMBER_OF_LINES)
        {
            throw new IndexOutOfRangeException("index");
        }

        return _lines.First(l => l.Index == index);
    }

    public Column GetColumn(int index)
    {
        if (index < 1 || index > NUMBER_OF_COLUMNS)
        {
            throw new IndexOutOfRangeException("index");
        }

        return _columns.First(c => c.Index == index);
    }

    public Square GetSquare(Position p)
    {
        return GetLine(p.Line).GetSquare(p.Column);
    }
}