﻿namespace Games;

public class GameTicTacToe : Game
{
    public GameTicTacToe()
    {
        Grid = new GridTicTacToe();
    }

    public void PlaceToken(Position position)
    {
        Grid.GetSquare(position).PlaceToken(CurrentPlayer);
        if (CurrentPlayerWon() || Draw())
        {
            IsPlaying = false;
        }
        EndPlayerTurn();
    }

    private bool Draw()
    {
        return Grid.IsFull();
    }

    protected override bool CurrentPlayerWon()
    {
        if (CurrentPlayerWonALine() || CurrentPlayerWonAColumn() || CurrentPlayerWonADiagonal())
        {
            Winner = CurrentPlayer;
            return true;
        }
        return false;
    }

    private bool CurrentPlayerWonALine()
    {
        for (int i = 1; i <= Grid.NUMBER_OF_LINES; i++)
        {
            var line = Grid.GetLine(i);
            if (AllAreOwnedByCurrentPlayer(line)) return true;
        }
        return false; 
    }

    private bool AllAreOwnedByCurrentPlayer(IReadOnlyCollection<Square> squares)
    {
        return squares.All(s => s.IsOwnedBy(CurrentPlayer));
    }

    private bool CurrentPlayerWonAColumn()
    {
        for (int i = 1; i <= Grid.NUMBER_OF_COLUMNS; i++)
        {
            if (AllAreOwnedByCurrentPlayer(Grid.GetColumn(i))) return true;            
        }
        return false;
    }

    private bool CurrentPlayerWonADiagonal()
    {
        var grid = (Grid as GridTicTacToe) ?? throw new GridMismatchException(); 

        return AllAreOwnedByCurrentPlayer(grid.GetFirstDiagonal()) 
              || AllAreOwnedByCurrentPlayer(grid.GetSecondDiagonal());
    }
}