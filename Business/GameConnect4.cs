﻿namespace Games;

public class GameConnect4 : Game
{
    public GameConnect4()
    {
        Grid = new GridConnect4();
    }

    private const int ALIGNED_TOKENS_NEEDED_FOR_WIN = 4; 
    public void PlaceToken(int columnIndex)
    {
        var grid = Grid as GridConnect4 ?? throw new GridMismatchException();
        
        grid.PlayInColumn(columnIndex, CurrentPlayer ?? throw new NullReferenceException());
        if (CurrentPlayerWon() || Draw())
        {
            IsPlaying = false; 
        }
        EndPlayerTurn();
    }

    protected override bool CurrentPlayerWon()
    {
        if(CurrentPlayerConnected4TokensOnColumn() || CurrentPlayerConnected4TokensOnLine()|| CurrentPlayerConnected4TokensOnDiagonal())
        {
            Winner = CurrentPlayer;
            return true;
        }
        return false;
    }
    
    private bool CurrentPlayerConnected4TokensOnColumn()
    {
        foreach (var column in Grid.GetColumns())
        {
            if (CurrentPlayerConnectedEnoughTokensInSquares(column))
            {
                return true;
            }
        }
        return false;
    }

    private bool CurrentPlayerConnected4TokensOnDiagonal()
    {
        var grid = Grid as GridConnect4 ?? throw new GridMismatchException();
        foreach (var line in grid.GetDiagonals(ALIGNED_TOKENS_NEEDED_FOR_WIN))
        {
            if (CurrentPlayerConnectedEnoughTokensInSquares(line))
            {
                return true;
            }
        }
        return false;
    }

    private bool CurrentPlayerConnected4TokensOnLine()
    {
        foreach (var line in Grid.GetLines())
        {
            if (CurrentPlayerConnectedEnoughTokensInSquares(line))
            {
                return true;
            }
        }
        return false;
    }

    private bool CurrentPlayerConnectedEnoughTokensInSquares(IReadOnlyCollection<Square> column)
    {
        int counter = 0;
        foreach (var square in column)
        {
            counter = square.IsOwnedBy(CurrentPlayer) ? counter + 1 : 0; 
            if (counter == ALIGNED_TOKENS_NEEDED_FOR_WIN) return true; 
        }
        return false;
    }
}