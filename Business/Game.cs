﻿using System.Collections.Generic;

namespace Games;

public abstract class Game
{
    public Player CurrentPlayer { get; private set; } 
    protected List<Player> Players { get; set; } = new List<Player>();
    public RectangularGrid Grid { get; protected set; }
    public virtual int MAX_NUMBER_OF_PLAYERS => 2;
    public virtual int MIN_NUMBER_OF_PLAYERS => 2; 
    public bool IsPlaying { get; protected set; }
    public Player? Winner { get; protected set; }

    public Game()
    {
        Grid = new GridTicTacToe();
        CurrentPlayer = null;
    }

    public void AddPlayer(Player p)
    {
        if (Players.Count < MAX_NUMBER_OF_PLAYERS)
        {
            Players.Add(p); 
        }
        if (CurrentPlayer is null)
        {
            CurrentPlayer = p; 
        }
    }

    public void Start()
    {
        if (Players.Count < MIN_NUMBER_OF_PLAYERS)
        {
            throw new Exception("Not enough players");
        }
        IsPlaying = true;
    }

    protected void EndPlayerTurn()
    {
        CurrentPlayer = Players.First(p => p != CurrentPlayer);
    }

    protected abstract bool CurrentPlayerWon();

    protected bool Draw()
    {
        return Grid.IsFull();
    }
}