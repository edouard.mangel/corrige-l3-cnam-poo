﻿using System.Collections.Generic;

namespace Games;

public class Square
{
    public readonly Position Position;
    public char Value { get; private set; }
    public bool IsEmpty => Value == ' ';

    public int Line => Position.Line;

    internal void PlaceToken(Player currentPlayer)
    {
        if (!IsEmpty)
        {
            throw new NonEmptyCellException($"Cell at {Position} is not empty.");
        }
        this.Value = currentPlayer.Token;
    }

    public int Column => Position.Column;

    public Square(Position position)
    {
        Position = position;
        Value = ' '; 
    }

    internal bool IsOwnedBy(Player currentPlayer)
    {
        return Value == currentPlayer.Token;
    }
}