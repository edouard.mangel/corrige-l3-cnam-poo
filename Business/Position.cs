﻿using System.Diagnostics.CodeAnalysis;

namespace Games;

public struct Position
{
    public readonly int Line;
    public readonly int Column;

    public Position(int line, int column)
    {
        Line = line;
        Column = column;
    }

    public override string ToString()
    {
        return $"Line : {Line}, Column : {Column}";
    }

    // Opérateurs nécessaires pour la comparaison de 2 structs. 
    public override bool Equals(object obj)
    {
        if (obj is null || !(obj is Position)) return false;
        return this.GetHashCode() == obj.GetHashCode();
    }

    public override int GetHashCode()
    {
        return Line.GetHashCode() ^ Column.GetHashCode();   
    }
}