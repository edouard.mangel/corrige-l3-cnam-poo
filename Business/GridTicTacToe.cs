﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Games;

public class GridTicTacToe : RectangularGrid
{
    public override int NUMBER_OF_LINES => 3;

    public override int NUMBER_OF_COLUMNS => 3;

    internal IReadOnlyCollection<Square> GetSecondDiagonal()
    {
        List<Square> result = new List<Square>();
        result.Add(GetSquare(new Position(1, 3)));
        result.Add(GetSquare(new Position(2, 2)));
        result.Add(GetSquare(new Position(3, 1)));
        return result;
    }

    internal IReadOnlyCollection<Square> GetFirstDiagonal()
    {
        List<Square> result = new List<Square>();
        result.Add(GetSquare(new Position(1, 1)));
        result.Add(GetSquare(new Position(2, 2)));
        result.Add(GetSquare(new Position(3, 3)));
        return result;
    }
}