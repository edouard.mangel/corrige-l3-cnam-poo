﻿using System;

namespace Games;

public class NonEmptyCellException : Exception
{
    public NonEmptyCellException(string? message) : base(message)
    {
    }
}