﻿using System.Runtime.Serialization;

namespace Games;

[Serializable]
internal class GridMismatchException : Exception
{
    public GridMismatchException()
    {
    }

    public GridMismatchException(string? message) : base(message)
    {
    }

    public GridMismatchException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected GridMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
}