﻿using static Games.GridConnect4;

namespace Games;

public class GridConnect4 : RectangularGrid
{
    public override int NUMBER_OF_LINES => 6;
    public override int NUMBER_OF_COLUMNS => 7;

    public void PlayInColumn(int columnIndex, Player p)
    {
        GetFirstEmptySquareInColumn(columnIndex).PlaceToken(p);
    }

    private Square GetFirstEmptySquareInColumn(int columnIndex)
    {
        return GetColumn(columnIndex).OrderBy(s => s.Line).FirstOrDefault(s => s.IsEmpty) ?? throw new ColumnFullException();
    }

    public IReadOnlyCollection<IReadOnlyCollection<Square>> GetDiagonals(int minSize)
    {
        var diagonals = new List<List<Square>>();
        diagonals.AddRange(GetDescendingDiagonals(minSize));
        diagonals.AddRange(GetAscendingDiagonals(minSize));

        return diagonals;
    }

    private bool IsInGrid(Position neighbourPosition)
    {
        return neighbourPosition.Line <= NUMBER_OF_LINES && neighbourPosition.Line >= 1 && neighbourPosition.Column >= 1 && neighbourPosition.Column <= NUMBER_OF_COLUMNS;
    }

    private IEnumerable<List<Square>> GetAscendingDiagonals(int minSize)
    {
        var list = new List<List<Square>>();
        foreach (var square in GetColumn(1))
        {
            list.Add(GetAscendingDiagonal(square));
        }
        for (int i = 2; i <= NUMBER_OF_COLUMNS - minSize + 1; i++)
        {
            Square firstSquare = GetSquare(new Position(1, i));
            list.Add(GetAscendingDiagonal(firstSquare));
        }
        return list.Where(l => l.Count >= minSize);
    }

    private IEnumerable<List<Square>> GetDescendingDiagonals(int minSize)
    {
        var list = new List<List<Square>>();
        foreach (var square in GetColumn(1))
        {
            list.Add(GetDescendingDiagonal(square));
        }
        for (int i = 2; i <= NUMBER_OF_COLUMNS - minSize + 1 ; i++)
        {
            Square firstSquare = GetSquare(new Position(NUMBER_OF_LINES, i));
            list.Add(GetDescendingDiagonal(firstSquare));
        }
        return list.Where(l => l.Count >= minSize);
    }

    private List<Square> GetDescendingDiagonal(Square square)
    {
        return GetLineInDirection(square, NeighbourDirection.BOTTOM_RIGHT);
    }

    private List<Square> GetAscendingDiagonal(Square square)
    {
        return GetLineInDirection(square, NeighbourDirection.TOP_RIGHT);
    }

    private List<Square> GetLineInDirection(Square square, NeighbourDirection direction)
    {
        var diagonal = new List<Square>();
        Position cursor = square.Position;
        while (IsInGrid(cursor))
        {
            diagonal.Add(GetSquare(cursor));
            cursor = SquaresHelper.GetNeighbourPosition(cursor, direction);
        }

        return diagonal;
    }

    public enum NeighbourDirection
    {
        TOP,
        TOP_RIGHT,
        RIGHT,
        BOTTOM_RIGHT,
        BOTTOM,
        BOTTOM_LEFT,
        LEFT,
        TOP_LEFT
    }
}

static class SquaresHelper
{
    public static Position GetNeighbourPosition(Position position, NeighbourDirection direction)
    {
        switch (direction)
        {
            case NeighbourDirection.TOP:
                return new Position(position.Line + 1, position.Column);
            case NeighbourDirection.TOP_LEFT:
                return new Position(position.Line + 1, position.Column - 1);
            case NeighbourDirection.LEFT:
                return new Position(position.Line, position.Column - 1);
            case NeighbourDirection.BOTTOM_LEFT:
                return new Position(position.Line + 1, position.Column - 1);
            case NeighbourDirection.BOTTOM:
                return new Position(position.Line - 1, position.Column);
            case NeighbourDirection.BOTTOM_RIGHT:
                return new Position(position.Line - 1, position.Column + 1);
            case NeighbourDirection.RIGHT:
                return new Position(position.Line, position.Column + 1);
            case NeighbourDirection.TOP_RIGHT:
                return new Position(position.Line + 1, position.Column + 1);
            default:
                throw new Exception("Direction not implemented");    
        }
    }
}