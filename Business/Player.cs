﻿using System.Collections.Generic;

namespace Games;

public class Player
{
    public readonly string Name;
    public readonly char Token;

    public Player(string name, char token)
    {
        Name = name;
        Token = token;
    }

}