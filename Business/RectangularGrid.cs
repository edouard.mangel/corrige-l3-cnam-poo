﻿namespace Games;

public abstract class RectangularGrid
{
    protected List<Square> _squares = new List<Square>();
    public RectangularGrid()
    {
        for (int i = 1; i <= NUMBER_OF_LINES; i++)
        {
            for (int j = 1; j <= NUMBER_OF_COLUMNS; j++)
            {
                _squares.Add(new Square(new Position(i, j)));
            }
        }
    }

    public virtual int NUMBER_OF_LINES => 0;

    internal bool IsFull()
    {
        foreach (var square in _squares)
        {
            if (square.IsEmpty)
            {
                return false;
            }
        }
        return true;
    }

    public virtual int NUMBER_OF_COLUMNS => 0;

    public IReadOnlyCollection<Square> GetColumn(int index)
    {
        if (index < 1 || index > NUMBER_OF_COLUMNS){
            throw new IndexOutOfRangeException("index");
        }
        return _squares.Where(c => c.Column == index)
                        .OrderBy(c => c.Column)
                        .ToList();
    }

    public IReadOnlyCollection<IReadOnlyCollection<Square>> GetColumns()
    {
        List<IReadOnlyCollection<Square>> columns = new List<IReadOnlyCollection<Square>>();
        for (int i = 1; i < NUMBER_OF_COLUMNS; i++)
        {
            columns.Add(GetColumn(i));
        }
        return columns;
    }

    internal IReadOnlyCollection<IReadOnlyCollection<Square>> GetLines()
    {
        List<IReadOnlyCollection<Square>> columns = new List<IReadOnlyCollection<Square>>();
        for (int i = 1; i < NUMBER_OF_LINES; i++)
        {
            columns.Add(GetLine(i));
        }
        return columns;
    }

    public IReadOnlyCollection<Square> GetLine(int index)
    {
        if (index < 1 || index > NUMBER_OF_LINES){
            throw new IndexOutOfRangeException("index");
        }
        return _squares.Where(s => s.Line == index)
                        .OrderBy(s => s.Column)
                        .ToList();
    }

    public Square GetSquare(Position p)
    {
        return _squares.Single(c => c.Line == p.Line && c.Column == p.Column);
    }
}   